#!/bin/bash

cp -r /data/input/files/. /task/

passed_message="Задание выполнено!"

if [ ! -f "/task/$solution_name.js" ] && [ ! -f "/task/$solution_name.py" ]; then
    echo "No solution file!"
    exit 1
fi

cd /task

echo "Running checker..."

if [ -f "/task/$solution_name.js" ]; then
    /trikStudio-checker/checker.sh "$task_name.qrs" "$solution_name.js"
else
    export TRIK_PYTHONPATH=$(python3 -c 'import sys; import os; print(os.pathsep.join(sys.path))');
    /trikStudio-checker/checker.sh "$task_name.qrs" "$solution_name.py"
fi

if [[ $? -ne 0 ]]; then
    echo "Checker error"
    exit 1
fi

for report in /task/reports/$task_name/*; do
    echo -n "$(basename "$report"):"
    grep "message" "$report"
done > full_report

passed_tests=0
failed_tests=0
# sorted_report=""
for report in /task/reports/$task_name/*; do
    if grep -q "$passed_message" "$report"
    then
        ((passed_tests++))
        # echo "$(basename "$report"): passed"
    else
        ((failed_tests++))
        # echo "$(basename "$report"): failed"
    fi
done > short_report

PRELIMINARY=$((${passed_tests}00 / 13))

RES="0.00"
if [ ${#PRELIMINARY} -eq 2 ]; then
  RES="0.${PRELIMINARY}"
else
  if [ ${#PRELIMINARY} -eq 1 ]; then
    RES="0.0${PRELIMINARY}"
  else if [ ${#PRELIMINARY} -eq 3 ]; then
    RES="1.00"
    fi
  fi
fi

echo "===== Full report ====="
sorted_report=$(sort full_report -V | column -t)
echo -e "$sorted_report"

total_tests=$((passed_tests + failed_tests))
echo "Total score: $passed_tests/$total_tests"


if [ "$full_repor_flag" = true ] ; then
    echo '{}' | jq --arg m "$(echo -e "$sorted_report")" --arg s $RES '. | {score: $s, msg: $m}' > "result.json"
else
    echo '{}' | jq --arg m "$(echo -e "$passed_tests of 13 tests passed!")" --arg s $RES '. | {score: $s, msg: $m}' > "result.json"
fi

cp result.json ../data/output/result.json

if [[ failed_tests -ne 0 ]]; then
    exit 1
fi
