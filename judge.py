import sys
import time
import random
import math



pi = 3.141592653589793

x = 8
d_x = 0
max_x = 8
min_x = 8
y = 8
d_y = 0
max_y = 8
min_y = 8

t = 0

dir = 0

copy_of_maze = []
used = []

maze = [[-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1],
[-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1],
[-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1],
[-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1],
[-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1],
[-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1],
[-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1],
[-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1],
[-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1],
[-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1],
[-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1],
[-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1],
[-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1],
[-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1],
[-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1],
[-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1],
[-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1]]

maze[8][8] = 0


angle = 180
cellSize = 70
wheelD = 5.6
track = 15.3
wallRegConst = 34
sensorNormal = 25

encTargetsleft = 0
encTargetsright = 0

motorLeft = brick.motor("M4")

motorRight = brick.motor("M3")

motorLeftEnc = brick.encoder("E4")

motorRightEnc = brick.encoder("E3")

encoderLeft = brick.encoder("E4").read

encoderRight = brick.encoder("E3").read

encoderLeftReverse = 1

encoderRightReverse = 1

sensorFront = brick.sensor("D1").read

sensorLeft = brick.sensor("A1").read

sensorRight = brick.sensor("A2").read

sensorBack = brick.sensor("D2").read


if (sensorRight() < 75):
  maze[y][x-1] = 1
if (sensorLeft() < 75):
  maze[y][x+1] = 1
if (sensorFront() < 75):
  maze[y+1][x] = 1
if (sensorBack() < 75):
  maze[y-1][x] = 1

point_arr = []

class Point():
  def __init__(self, y, x):
    self.y = y
    self.x = x
  def forw_back(self, d1 = 0, d4 = 0):
    self.d1 = d1
    self.d4 = d4
  def right_left(self, d2 = 0, d3 = 0):
    self.d2 = d2
    self.d3 = d3


def generate_point():
  global point_arr, y, x, dir
  if (dir==0):
    p1 = Point(y, x)
    p1.forw_back(sensorFront(), sensorBack())
    turnDegreesEnc(90)
    p1.right_left(sensorFront(), sensorBack())
    turnDegreesEnc(-90)
    point_arr.append(p1)
  if (dir==1):
    p1 = Point(y, x)
    p1.right_left(sensorFront(), sensorBack())
    turnDegreesEnc(90)
    p1.forw_back(sensorBack(), sensorFront())
    turnDegreesEnc(-90)
    point_arr.append(p1)
  if (dir==2):
    p1 = Point(y, x)
    p1.forw_back(sensorBack(), sensorFront())
    turnDegreesEnc(90)
    p1.right_left(sensorBack(), sensorFront())
    turnDegreesEnc(-90)
    point_arr.append(p1)
  if (dir==3):
    p1 = Point(y, x)
    p1.right_left(sensorBack(), sensorFront())
    turnDegreesEnc(90)
    p1.forw_back(sensorFront(), sensorBack())
    turnDegreesEnc(-90)
    point_arr.append(p1)
  #print("[", p1.x, p1.y, p1.d1, p1.d2, p1.d3, p1.d4, "]", sep=' ')


def moveEnc(enc):
  global encTargetsleft
  global encTargetsright

  if (enc == 0):
    stopMotors()
    return

  if enc > 0:
    direction = 1
  else:
    direction = -1

  enc = enc

  encTargetsleft += enc

  encTargetsright += enc

  eL = encoderLeft()

  eR = encoderRight()

  errorLeft = (encTargetsleft - eL) * direction

  errorRight = (encTargetsright - eR) * direction

  kPE = 0.1 * direction

  kDE = 1 * direction

  errorEncoder = errorLeft - errorRight

  last_errorEncoder = errorEncoder

  speed = 20

  while (errorRight > 0 and errorLeft > 0):

    eL = encoderLeft()

    eR = encoderRight()



    errorLeft = (encTargetsleft - eL) * direction

    errorRight = (encTargetsright - eR) * direction



    errorEncoder = errorLeft - errorRight

    correction = errorEncoder * kPE + (errorEncoder - last_errorEncoder) * kDE



    setMotorSpeed( (speed * direction + correction), (speed * direction - correction))



    last_errorEncoder = errorEncoder

    script.wait(10)

  stopMotors()


def get_gyro():
  return brick.gyroscope().read()[6] / 1000 + 180

def sm2cpr(sm):
  return sm / pi / wheelD * 360

def driveCellSim():
  global encTargetsleft
  global encTargetsright
  global angle

  backward_sm = 0
  enc = sm2cpr(cellSize + abs(backward_sm))
  eS  = sm2cpr(58 + abs(backward_sm))

  side_sens = [-1, -1]



  vLeft = sensorLeft()

  vRight = sensorRight()

  if vLeft > wallRegConst: 
    vLeft = wallRegConst
  if vRight > wallRegConst:
    vRight = wallRegConst
  errWall = vRight - vLeft
  lastErr = errWall
  kP = 0.5
  kD = 5
  encTargetsleft += enc
  encTargetsright += enc
  eL = encoderLeft()
  eR = encoderRight()
  seL = eL
  seR = eR
  pL = eL - seL
  pR = eR - seR



  while (eL + eR < encTargetsleft + encTargetsright and sensorFront() > 38):

    vLeft = sensorLeft()
    vRight = sensorRight()

    pL = eL - seL

    pR = eR - seR


    if (pL + pR > 2 * eS and side_sens[0] == -1):
      side_sens = [vLeft, vRight]
      #print(side_sens)



    if (vLeft > wallRegConst + 5):
      vLeft = wallRegConst

    if (vRight > wallRegConst + 5):
      vRight = wallRegConst



    errWall = vRight - vLeft

    #correction = errWall * kP + (errWall - lastErr) * kD

    correction = angle - get_gyro()
    if(abs(correction) > 10):
      correction = 360 - (-1*correction)

    setMotorSpeed(30 + correction, 30 - correction)

    eL = encoderLeft()

    eR = encoderRight()

    script.wait(50)

    lastErr = errWall

  if (sensorFront() < 38):

    while (sensorFront() > 34):

      vLeft = sensorLeft()

      vRight = sensorRight()

      if (vLeft > wallRegConst + 2):
        vLeft = wallRegConst

      if (vRight > wallRegConst + 2):
        vRight = wallRegConst

      errWall = vRight - vLeft

      correction = errWall * kP + (errWall - lastErr) * kD

      setMotorSpeed(30 + correction, 30 - correction)

      eL = encoderLeft()

      eR = encoderRight()

      script.wait(10)

      lastErr = errWall


  stopMotors()

  change_x_y()

  return side_sens

def empty_space(t_y, t_x):
  global used, maze
  if (maze[t_y][t_x]==2 or maze[t_y][t_x]==1):
    used[t_y][t_x] = 1
    return 1
  elif (maze[t_y][t_x]==0):
    return -1
  else:
    used[t_y][t_x] = 1
    
    if (t_y+1>max_y):
      return -1
    elif(used[t_y+1][t_x]==0 and t_y+1<=max_y):
      if (empty_space(t_y+1, t_x) == -1):
        return -1
      
    if(t_x+1>max_x):
      return -1
    elif (used[t_y][t_x+1]==0 and t_x+1<=max_x):
      if (empty_space(t_y, t_x+1) == -1):
        return -1

    if(t_y-1<min_y):
      return -1
    elif (used[t_y-1][t_x]==0 and t_y-1>=min_y):
      if (empty_space(t_y-1, t_x) == -1):
        return -1

    if(t_x-1<min_x):
      return -1
    elif (used[t_y][t_x-1]==0 and t_x-1>=min_x):
      if (empty_space(t_y, t_x-1) == -1):
        return -1

    return 1

def replace_empty_space():
  global used, maze
  for i in range(16):
    for j in range(16):
      if (used[i][j]==1 and maze[i][j]==-1):
        maze[i][j] = 2
  return
  



def complete_check():
  global maze
  for i in range(8):
    for j in range(8):
      if (maze[min_y + i][min_x + j] == 1):
        maze[min_y + i][min_x + j] = 2

  for i in range(8):
    for j in range(8):
      if (maze[min_y + i][min_x + j] == -1):
        arr_for_bfs()
        if (empty_space(min_y + i, min_x + j) == 1):
          replace_empty_space()
        
      

  for i in range(8):
    for j in range(8):
      if (maze[min_y + i][min_x + j] == -1):
        return 0
  return 1


def maze_check(symb):
  global maze, t, dir
  if (dir==0):
    if (symb == "FFF"):
      if (y+3 <= max_y):
        if (maze[y+3][x] == 0 or maze[y+1][x] == 1 or maze[y+2][x] == 1 or maze[y+3][x] == 1 or maze[y+3][x] == 2 or maze[y+2][x] == 2 or maze[y+1][x] == 2):
          return 0
      else:
        return 0

    if(symb == "RRR"):
      if (x-3 >= min_x):
        if (maze[y][x-3] == 0 or maze[y][x-1] >= 1 or maze[y][x-2] >= 1 or maze[y][x-3] >= 1):
          return 0
      else:
        return 0

    if(symb == "LLL"):
      if (x+3 <= max_x):
        if (maze[y][x+3] == 0 or maze[y][x+1] >= 1 or maze[y][x+2] >= 1 or maze[y][x+3] >= 1):
          return 0
      else:
        return 0

    if (symb == "FF"):
      if (y+2 <= max_y):
        if (maze[y+2][x] == 0 or maze[y+1][x] == 1 or maze[y+2][x] == 1 or maze[y+1][x] == 2 or maze[y+2][x] == 2):
          return 0
      else:
        return 0

    if(symb == "RR"):
      if (x-2 >= min_x):
        if (maze[y][x-2] == 0 or maze[y][x-1] == 1 or maze[y][x-2] == 1 or maze[y][x-1] == 2 or maze[y][x-2] == 2):
          return 0
      else:
        return 0

    if(symb == "LL"):
      if (x+2 <= max_x):
        if (maze[y][x+2] == 0 or maze[y][x+1] == 1 or maze[y][x+2] == 1 or maze[y][x+1] == 2 or maze[y][x+2] == 2):
          return 0
      else:
        return 0

    if (symb == 'R'):
      if (maze[y][x-1] == 0):
        return 0
    if (symb == 'L'):
      if (maze[y][x+1] == 0):
        return 0
    if (symb == 'F'):
      if (maze[y+1][x] == 0):
        return 0
    return 1


  if (dir==1):
    if (symb == "FFF"):
      if (x-3 >= min_x):
        if (maze[y][x-3] == 0 or maze[y][x-1] >= 1 or maze[y][x-2] >= 1 or maze[y][x-3] >= 1):
          return 0
      else:
        return 0

    if(symb == "RRR"):
      if (y-3 >= min_y):
        if (maze[y-3][x] == 0 or maze[y-1][x] >= 1 or maze[y-2][x] >= 1 or maze[y-3][x] >= 1):
          return 0
      else:
        return 0

    if(symb == "LLL"):
      if (y+3 <= max_y):
        if (maze[y+3][x] == 0 or maze[y+1][x] >= 1 or maze[y+2][x] >= 1 or maze[y+3][x] >= 1):
          return 0
      else:
        return 0


    if (symb == "FF"):
      if (x-2 >= min_x):
        if (maze[y][x-2] == 0 or maze[y][x-1] == 1 or maze[y][x-2] == 1 or maze[y][x-1] == 2 or maze[y][x-2] == 2):
          return 0
      else:
        return 0

    if(symb == "RR"):
      if (y-2 >= min_y):
        if (maze[y-2][x] == 0 or maze[y-1][x] == 1 or maze[y-2][x] == 1 or maze[y-1][x] == 2 or maze[y-2][x] == 2):
          return 0
      else:
        return 0

    if(symb == "LL"):
      if (y+2 <= max_y):
        if (maze[y+2][x] == 0 or maze[y+1][x] == 1 or maze[y+2][x] == 1 or maze[y+1][x] == 2 or maze[y+2][x] == 2):
          return 0
      else:
        return 0

    if (symb == 'R'):
      if (maze[y-1][x] == 0):
        return 0
    if (symb == 'L'):
      if (maze[y+1][x] == 0):
        return 0
    if (symb == 'F'):
      if (maze[y][x-1] == 0):
        return 0
    return 1

  if (dir==2):
    if (symb == "FFF"):
      if (y-3 >= min_y):
        if (maze[y-3][x] == 0 or maze[y-1][x] >= 1 or maze[y-2][x] >= 1 or maze[y-3][x] >= 1):
          return 0
      else:
        return 0

    if(symb == "RRR"):
      if (x+3 <= max_x):
        if (maze[y][x+3] == 0 or maze[y][x+1] >= 1 or maze[y][x+2] >= 1 or maze[y][x+3] >= 1):
          return 0
      else:
        return 0

    if(symb == "LLL"):
      if (x-3 >= min_x):
        if (maze[y][x-3] == 0 or maze[y][x-1] >= 1 or maze[y][x-2] >= 1 or maze[y][x-3] >= 1):
          return 0
      else:
        return 0


    if (symb == "FF"):
      if (y-2 >= min_y):
        if (maze[y-2][x] == 0 or maze[y-1][x] == 1 or maze[y-2][x] == 1 or maze[y-1][x] == 2 or maze[y-2][x] == 2):
          return 0
      else:
        return 0

    if(symb == "RR"):
      if (x+2 <= max_x):
        if (maze[y][x+2] == 0 or maze[y][x+1] == 1 or maze[y][x+2] == 1 or maze[y][x+1] == 2 or maze[y][x+2] == 2):
          return 0
      else:
        return 0

    if(symb == "LL"):
      if (x-2 >= min_x):
        if (maze[y][x-2] == 0 or maze[y][x-1] == 1 or maze[y][x-2] == 1 or maze[y][x-1] == 2 or maze[y][x-2] == 2):
          return 0
      else:
        return 0

    if (symb == 'R'):
      if (maze[y][x+1] == 0):
        return 0
    if (symb == 'L'):
      if (maze[y][x-1] == 0):
        return 0
    if (symb == 'F'):
      if (maze[y-1][x] == 0):
        return 0
    return 1


  if (dir==3):
    if (symb == "FFF"):
      if (x+3 <= max_x):
        if (maze[y][x+3] == 0 or maze[y][x+1] >= 1 or maze[y][x+2] >= 1 or maze[y][x+3] >= 1):
          return 0
      else:
        return 0

    if(symb == "RRR"):
      if (y+3 <= max_y):
        if (maze[y+3][x] == 0 or maze[y+1][x] >= 1 or maze[y+2][x] >= 1 or maze[y+3][x] >= 1):
          return 0
      else:
        return 0

    if(symb == "LLL"):
      if (y-3 >= min_y):
        if (maze[y-3][x] == 0 or maze[y-1][x] >= 1 or maze[y-2][x] >= 1 or maze[y-3][x] >= 1):
          return 0
      else:
        return 0


    if (symb == "FF"):
      if (x+2 <= max_x):
        if (maze[y][x+2] == 0 or maze[y][x+1] == 1 or maze[y][x+2] == 1 or maze[y][x+1] == 2 or maze[y][x+2] == 2):
          return 0
      else:
        return 0

    if(symb == "RR"):
      if (y+2 <= max_y):
        if (maze[y+2][x] == 0 or maze[y+1][x] == 1 or maze[y+2][x] == 1 or maze[y+1][x] == 2 or maze[y+2][x] == 2):
          return 0
      else:
        return 0

    if(symb == "LL"):
      if (y-2 >= min_y):
        if (maze[y-2][x] == 0 or maze[y-1][x] == 1 or maze[y-2][x] == 1 or maze[y-1][x] == 2 or maze[y-2][x] == 2):
          return 0
      else:
        return 0

    if (symb == 'R'):
      if (maze[y+1][x] == 0):
        return 0
    if (symb == 'L'):
      if (maze[y-1][x] == 0):
        return 0
    if (symb == 'F'):
      if (maze[y][x+1] == 0):
        return 0
    return 1


def change_x_y():
  global x, max_x, min_x, y, max_y, min_y, dir, d_x, d_y, maze

  if (dir==0):
    y = y + 1
    if (sensorRight() < 75):
      maze[y][x-1] = 1
    if (sensorLeft() < 75):
      maze[y][x+1] = 1
    if (sensorFront() < 75):
      maze[y+1][x] = 1
    if (sensorBack() < 75):
      maze[y-1][x] = 1


  if (dir==1):
    x = x - 1
    if (sensorRight() < 75):
      maze[y-1][x] = 1
    if (sensorLeft() < 75):
      maze[y+1][x] = 1
    if (sensorFront() < 75):
      maze[y][x-1] = 1
    if (sensorBack() < 75):
      maze[y][x+1] = 1


  if (dir==2):
    y = y - 1
    if (sensorRight() < 75):
      maze[y][x+1] = 1
    if (sensorLeft() < 75):
      maze[y][x-1] = 1
    if (sensorFront() < 75):
      maze[y-1][x] = 1
    if (sensorBack() < 75):
      maze[y+1][x] = 1


  if (dir==3):
    x = x + 1
    if (sensorRight() < 75):
      maze[y+1][x] = 1
    if (sensorLeft() < 75):
      maze[y-1][x] = 1
    if (sensorFront() < 75):
      maze[y][x+1] = 1
    if (sensorBack() < 75):
      maze[y][x-1] = 1

  if (maze[y][x]!=0):
    generate_point()

  maze[y][x] = 0

  if (x > max_x):
    max_x = x
  if (x < min_x):
    min_x = x
  if (y > max_y):
    max_y = y
  if (y < min_y):
    min_y = y



  d_x = max_x - min_x
  d_y = max_y - min_y
  




def turnDegreesEnc(degrees):
  global encTargetsleft
  global encTargetsright
  global angle
  global dir

  if(degrees == 90):
    dir = (dir + 1)%4
  elif(degrees == -90):
    dir = (dir-1+4)%4
  elif(degrees == 180):
    dir = (dir+2)%4
  
  angle = (angle + degrees+360)%360

  moveEnc(sm2cpr(5))

  if (degrees == 0): 
    stopMotors()
    return

  speedMax = 0

  speedStart = 20

  speedEnd = 20



  enc = sm2cpr( (pi * track) * degrees / 360)

  encRampUp = abs(enc/5)

  encRampDown = abs(enc/5)

  
  if enc > 0:
    direction = -1
  else:
    direction = 1

  speed = speedStart



  encTargetsleft += abs(enc) * -direction

  encTargetsright += abs(enc) * direction



  encP = -0.1

  encD = -1



  tL = encTargetsleft

  tR = encTargetsright

  

  eL = encoderLeft()

  eR = encoderRight()



  error = tL - eL + (tR - eR)

  errorL = tL - eL

  errorR = tR - eR



  last_errorL = errorL

  last_errorR = errorR



  correctionL = errorL * encP + (errorL - last_errorL) * encD

  correctionR = errorR * encP + (errorR - last_errorR) * encD



  last_error = error

  

  while ( (eL < tL and eR > tR and direction == -1) or (eL > tL and eR < tR and direction == 1) ):

    eL = encoderLeft()

    eR = encoderRight()

    error = (tL - eL + tR - eR) * direction

    correction = error * encP + (error - last_error) * encD

    last_error = error



    errorL = tL - eL

    errorR = tR - eR



    correctionL = errorL * encP + (errorL - last_errorL) * encD

    correctionR = errorR * encP + (errorR - last_errorR) * encD

    setMotorSpeed(-(speed + correction) * direction, (speed - correction) * direction)		



    last_errorL = errorL

    last_errorR = errorR	

    script.wait(10)
    
  err = angle - get_gyro()
  if(abs(err) > 10):
    err = 360 - (-1*err)
  
  while(abs(err)>1):
    err = angle - get_gyro()
    if(abs(err) > 10):
      err = 360 - (-1*err)
    setMotorSpeed(err, -err)
    script.wait(50)

  moveEnc(-1*sm2cpr(5))

  stopMotors()


def stopMotors():
  setMotorSpeed(0, 0)
  return


def setMotorSpeed(lS, rS):
  bool = 0
  if bool == 0:

      motorLeft.setPower(lS, 0)

      motorRight.setPower(rS, 0)

  else:

      motorLeft.setPower(lS)

      motorRight.setPower(rS)

  return

def arr_for_bfs():
  global maze, copy_of_maze, used

  used = []

  for i in range(16):
    temp = []
    for j in range(16):
      temp.append(0)
    used.append(temp)

  #print(copy_of_maze)
  return


def localize():
  global d_x, d_y, maze, dir
  while(d_x < 7 or d_y < 7):
    if(sensorFront() > 75 and maze_check('F')):
      driveCellSim()
    elif(sensorRight() > 75 and maze_check('R')):
      turnDegreesEnc(90)
      driveCellSim()
    elif(sensorLeft() > 75 and maze_check('L')):
      turnDegreesEnc(-90)
      driveCellSim()
    elif(maze_check("FF")):
      driveCellSim()
      driveCellSim()
    elif(sensorRight() > 75 and maze_check("RR")):
      turnDegreesEnc(90)
      driveCellSim()
      driveCellSim()
    elif(sensorLeft() > 75 and maze_check("LL")):
      turnDegreesEnc(-90)
      driveCellSim()
      driveCellSim()
    elif(maze_check("FFF")):
      driveCellSim()
      driveCellSim()
    elif(sensorRight() > 75 and maze_check("RRR")):
      turnDegreesEnc(90)
      driveCellSim()
      driveCellSim()
    elif(sensorLeft() > 75 and maze_check("LLL")):
      turnDegreesEnc(-90)
      driveCellSim()
      driveCellSim()
    elif(sensorFront() > 75):
      driveCellSim()
    elif(sensorRight() > 75):
      turnDegreesEnc(90)
      driveCellSim()
    elif(sensorLeft() > 75):
      turnDegreesEnc(-90)
      driveCellSim()
    else:
      turnDegreesEnc(180)

  #print("x = ", x - min_x)
  #print("y = ", y - min_y)

  return

def ride_maze():
  global d_x, d_y, maze
  right = 1
  k = 0

  localize()

  while(complete_check()==0):
    if (right):
      if(sensorFront() > 75 and maze_check('F')):
        k = 0
        driveCellSim()
      elif(sensorRight() > 75 and maze_check('R')):
        k = 0
        turnDegreesEnc(90)
        driveCellSim()
      elif(sensorLeft() > 75 and maze_check('L')):
        k = 0
        turnDegreesEnc(-90)
        driveCellSim()
      elif(maze_check("FF")):
        k = 0
        driveCellSim()
        driveCellSim()
      elif(sensorRight() > 75 and maze_check("RR")):
        k = 0
        turnDegreesEnc(90)
        driveCellSim()
        driveCellSim()
      elif(sensorLeft() > 75 and maze_check("LL")):
        k = 0
        turnDegreesEnc(-90)
        driveCellSim()
        driveCellSim()
      elif(maze_check("FFF")):
        k = 0
        driveCellSim()
        driveCellSim()
      elif(sensorRight() > 75 and maze_check("RRR")):
        k = 0
        turnDegreesEnc(90)
        driveCellSim()
        driveCellSim()
      elif(sensorLeft() > 75 and maze_check("LLL")):
        k = 0
        turnDegreesEnc(-90)
        driveCellSim()
        driveCellSim()
      elif(sensorFront() > 75):
        driveCellSim()
        k = k + 1
      elif(sensorRight() > 75):
        turnDegreesEnc(90)
        driveCellSim()
        k = k + 1
      elif(sensorLeft() > 75):
        turnDegreesEnc(-90)
        driveCellSim()
        k = k + 1
      else:
        turnDegreesEnc(180)

      if (k==4):
        k = 0
        right = 0

    else:

      if(sensorFront() > 75 and maze_check('F')):
        driveCellSim()
      elif(sensorLeft() > 75 and maze_check('L')):
        turnDegreesEnc(-90)
        driveCellSim()
      elif(sensorRight() > 75 and maze_check('R')):
        turnDegreesEnc(90)
        driveCellSim()
      elif(maze_check("FF")):
        driveCellSim()
        driveCellSim()
      elif(sensorLeft() > 75 and maze_check("LL")):
        turnDegreesEnc(-90)
        driveCellSim()
        driveCellSim()
      elif(sensorRight() > 75 and maze_check("RR")):
        turnDegreesEnc(90)
        driveCellSim()
        driveCellSim()
      elif(maze_check("FFF")):
        driveCellSim()
        driveCellSim()
      elif(sensorLeft() > 75 and maze_check("LLL")):
        turnDegreesEnc(-90)
        driveCellSim()
        driveCellSim()
      elif(sensorRight() > 75 and maze_check("RRR")):
        turnDegreesEnc(90)
        driveCellSim()
        driveCellSim()
      elif(sensorFront() > 75):
        driveCellSim()
      elif(sensorLeft() > 75):
        turnDegreesEnc(-90)
        driveCellSim()  
      elif(sensorRight() > 75):
        turnDegreesEnc(90)
        driveCellSim()
      else:
        turnDegreesEnc(180)


  #print(maze)
  return

def find_min():
  global min_x, min_y, point_arr
  f_min_y_1 = 4
  f_min_x_1 = 4
  f_min_y_2 = 4
  f_min_x_2 = 4
  p1 = Point(0, 0)
  p2 = Point(0, 0)
  p3 = Point(0, 0)
  p4 = Point(0, 0)
  bl = 0

  for i in range(4):
    for j in range(8):
      if(maze[min_y+i][min_x+j]==2):
        f_min_y_1 = i
        p1 = Point(min_y+i, min_x+j)
        bl = 1
        break
    if (bl):
      break

  bl = 0

  for i in range(7, 3, -1):
    for j in range(8):
      if(maze[min_y+i][min_x+j]==2):
        f_min_y_2 = 7-i
        p2 = Point(min_y+i, min_x+j)
        bl = 1
        break
    if (bl):
      break

  bl = 0

  for i in range(4):
    for j in range(8):
      if(maze[min_y+j][min_x+i]==2):
        f_min_x_1 = i
        p3 = Point(min_y+j, min_x+i)
        bl = 1
        break
    if (bl):
      break

  bl = 0

  for i in range(7, 3, -1):
    for j in range(8):
      if(maze[min_y+j][min_x+i]==2):
        f_min_x_2 = 7-i
        p4 = Point(min_y+j, min_x+i)
        bl = 1
        break
    if (bl):
      break


  m = min(min(f_min_x_1, f_min_x_2), min(f_min_y_1, f_min_y_2))
  m_dist = 560
  temp_dist = 0

  print(m)

  if (f_min_y_1 == m):
    if (m==0):
      i = 0
      j = 0
      while (maze[p1.y + i][p1.x] != 0):
        i = i + 1
      for p in point_arr:
        if (p.y == p1.y + i and p.x == p1.x):
          temp_dist = p.d4
          break
      if (p1.x - 1 >= min_x):
        for p in point_arr:
          if (p.y == p1.y + i and p.x == p1.x - 1):
            temp_dist = p.d4 - temp_dist - 70
            break
      else:
        while (maze[p1.y][p1.x+j] != 0):
          j = j + 1
        for p in point_arr:
          if (p.y == p1.y + i and p.x == p1.x + j):
            temp_dist = p.d4 - temp_dist - 70
            break
    else:
      for p in point_arr:
        if (p.y == p1.y - 1 and p.x == p1.x):
          temp_dist = p.d4 + p.d1
          break
    
    if (temp_dist < m_dist):
      m_dist = temp_dist
      temp_dist = 0

  if (f_min_y_2 == m):
    if (m==0):
      i = 0
      j = 0
      while (maze[p2.y + i][p2.x] != 0):
        i = i - 1
      for p in point_arr:
        if (p.y == p2.y + i and p.x == p2.x):
          temp_dist = p.d1
          break
      if (p2.x - 1 >= min_x):
        for p in point_arr:
          if (p.y == p2.y + i and p.x == p2.x - 1):
            temp_dist = p.d1 - temp_dist - 70
            break
      else:
        while (maze[p2.y][p2.x+j] != 0):
          j = j + 1
        for p in point_arr:
          if (p.y == p2.y + i and p.x == p2.x + j):
            temp_dist = p.d1 - temp_dist - 70
            break
    else:
      for p in point_arr:
        if (p.y == p2.y + 1 and p.x == p2.x):
          temp_dist = p.d4 + p.d1
          break
    
    if (temp_dist < m_dist):
      m_dist = temp_dist
      temp_dist = 0

  if (f_min_x_1 == m):
    if (m==0):
      i = 0
      j = 0
      while (maze[p3.y][p3.x+i] != 0):
        i = i + 1
      for p in point_arr:
        if (p.y == p3.y and p.x == p3.x + i):
          temp_dist = p.d2
          break
      if (p3.y - 1 >= min_y):
        for p in point_arr:
          if (p.y == p3.y - 1 and p.x == p3.x + i):
            temp_dist = p.d2 - temp_dist - 70
            break
      else:
        while (maze[p3.y + j][p3.x] != 0):
          j = j + 1
        for p in point_arr:
          if (p.y == p3.y + j and p.x == p3.x + i):
            temp_dist = p.d2 - temp_dist - 70
            break
    else:
      for p in point_arr:
        if (p.y == p3.y and p.x == p3.x - 1):
          temp_dist = p.d2 + p.d3
          break
    
    if (temp_dist < m_dist):
      m_dist = temp_dist
      temp_dist = 0
    
  if (f_min_x_2 == m):
    if (m==0):
      i = 0
      j = 0
      while (maze[p4.y][p4.x+i] != 0):
        i = i - 1
      for p in point_arr:
        if (p.y == p4.y and p.x == p4.x + i):
          temp_dist = p.d3
          break
      if (p4.y - 1 >= min_y):
        for p in point_arr:
          if (p.y == p4.y - 1 and p.x == p4.x + i):
            temp_dist = p.d3 - temp_dist - 70
            break
      else:
        while (maze[p4.y + j][p4.x] != 0):
          j = j + 1
        for p in point_arr:
          if (p.y == p4.y + j and p.x == p4.x + i):
            temp_dist = p.d3 - temp_dist - 70
            break
    else:
      for p in point_arr:
        if (p.y == p4.y and p.x == p4.x + 1):
          temp_dist = p.d3 + p.d2
          break
    
    if (temp_dist < m_dist):
      m_dist = temp_dist
      temp_dist = 0

  
  print(m_dist)
  brick.display().clear()
  brick.display().addLabel(m_dist, 1, 1)
  brick.display().redraw()

  return





class Program():
  __interpretation_started_timestamp__ = time.time() * 1000

  def execMain(self):
    brick.gyroscope().calibrate(4000)
    
    generate_point()
    ride_maze()
    find_min()
    
    
    brick.stop()
    return

def main():
  program = Program()
  program.execMain()

if __name__ == '__main__':
  main()
