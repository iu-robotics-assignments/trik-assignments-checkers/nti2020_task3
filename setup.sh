#!/bin/bash

if [ ! -f "/task/$task_name.qrs" ]; then
    echo "No .qrs file!"
    exit 1
fi

if [ ! -d "/task/fields/$task_name" ]; then
    echo "No fields directory!"
    exit 1
fi

# Creating empty input files for each field (if it does not have yet)
for field in /task/fields/$task_name/*.xml; do
    if [ ! -d "${field%.*}.txt" ]; then
        touch "${field%.*}.txt"
    fi
done

echo "Fields have been successfully copied!"

./task/start_checker.sh