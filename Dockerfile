FROM trikset/checker-stepik:20201214

ARG full_repor_flag=true
ARG task_name_arg=task
ARG solution_name_arg=solution

ENV full_repor_flag=$full_repor_flag
ENV task_name=$task_name_arg
ENV solution_name=$solution_name_arg


RUN apt update
RUN apt-get -y install python3
RUN export TRIK_PYTHONPATH=$(python3 -c 'import sys; import os; print(os.pathsep.join(sys.path))');
RUN apt-get install -y bsdmainutils git jq

COPY . .

RUN mkdir /xdg-runtime && chmod 700 /xdg-runtime
ENV XDG_RUNTIME_DIR=/xdg-runtime

RUN chmod +x /setup.sh
RUN chmod +x /task/start_checker.sh

CMD bash setup.sh
